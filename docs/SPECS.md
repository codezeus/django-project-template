# Specs

A list of models and probable fields.

# User

- Username (For slug)
- First Name / Last Name
- Location
- Friends (M2M)
- Optional Below:
    - Personality
    - Languages Spoken
    - Age
    - Ethnicity

# Friendship

- Friend1
- Friend2
- Created

# Friendship_Request

- Friend1
- Friend2
- Accepted
- Created

# Reported_User (We should allow moms to report suspicious activity)

- User
- Reason
- Created

# Child

- Name
- Age (Or Birthdate to keep it up to date)
- Gender
- Anything Else
- Parent (ForeignKey)

# Group

- Title
- Description
- Private
- Members (M2M)

# Group_User

- Group
- User
- Admin (boolean)

# Group_Post

- Group
- User
- Content

# Group_Invite

- Group
- User

# Event

- Title
- Event_Type
- Description
- Location
- Date
- Time + (Timezone)
- Max People
- Members (M2M)

# Event_User

- Event
- User
- Admin (boolean)

# Event_Post

- Event
- User
- Content

# Event_Invite

- Event
- User

# Event_Type

- Title

# RSVP

- Event
- User
- Coming
- TotalGuests
