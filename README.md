# IMPORTANT

Please use the open source repository: https://github.com/codezeus/django-project-template

# {{project_name}}

The following project provides an excellent start for you to begin a Django
project with a modern frontend build kit enabled. By using this project
template, you will be able to jump directly into your application without
worrying about the small details in setting things up. Out of the box, you'll
have the following features enabled:

- Django 1.8a1: The latest version of Django
- User authentication, authorization, and management ready to go
- User profiles with support for an Avatar
- Testing with py.test and coverage testing with Python coverage
- Smart environment specific configuration
- CoffeeScript and SCSS for frontend efficiency
- Build script using Gulp
- Browser LiveReload for .coffee, .scss, and Django Template files
- Templates written in Bootstrap 3 with Crispy Forms support for forms

All of this is ready to go and will be loaded on your first runserver command.

## Getting Started

### Set up your environment

Before getting anything done, you need to start by setting up your working
directory. We recommend virtualenv and virtualenvwrapper for this process and
a detailed introduction can be found [here](http://programeveryday.com/post/setting-up-a-python-development-environment-to-make-developing-a-breeze/).

Once you have this setup, you're ready to install the dependencies.

### Install Django

Using pip, we can install Django itself in order to use the `startproject`
command. We can do this by running:

```
$ pip install Django==1.8a1
```

This will build Django into our virtualenv and give us full access to the
management commands.

### Use Project Template

With Django we can now use this template to build our skeleton project. We do
so by running (NOTE: Change the `project_name` portion of the following
command to your project name.):

```
$ django-admin.py startproject --template=https://bitbucket.org/codezeus/django-project-template/get/cfe89a700f99.zip --extension=py,gitignore,json,md,sh,js,example --name=postactivate project_name
```

This will clone the repo and build the skelton into a directory specified by the
name you supplied. Personally, we like to move this new directory down a level
and to do so you can run:

```
$ mv project_name tmp && cp -r tmp/* . && rm -rf tmp
```

This will temporarily rename the directory, copy all files within it to the
current working directory, and remove the temporary directory.

### Add a settings/local.py file

In the template, you will need to make sure that you have a working
`project_name/settings/local.py` file available. To do so, you can run:

```
$ cp {{project_name}}/settings/local.py.example {{project_name}}/settings/local.py
```

For security sake, we will then want to edit the local.py.example file to
remove the SECRET_KEY from version control. We can edit
`{{project_name}}/settings/local.py.example` and remove the SECRET_KEY string.

### Install Dependencies

We will be using the Python Pillow library and to do that we will need to make
sure that we have the correct packages installed. You can find those packages
[here](http://pillow.readthedocs.org/installation.html).

Once they're installed, we can get the NPM modules installed. We're assuming
that you have `nodejs` and `npm` installed.

```
$ npm install
$ npm install -g gulp
$ npm install -g coffee-script
```

These will download all frontend dependencies for us to successfully run Gulp.
We can now download the projects requirements to the virtualenv by running:

```
$ pip install -r requirements/development.pip
```

Our final step should be to make the relevant migrations and migrate the
files.

```
$ django-admin.py makemigrations
$ django-admin.py migrate
```

If all goes well, we will be able to follow the development notes in the next
section.

## Development Notes

The following notes detail how we can work with his project.

### Running the Project

When working with this application, there is a frontend and backend which can
interact with each other and allow a very smooth development experience. It's
best to have three terminal buffers open at once. In our first terminal, we
will want to run Django:

```
$ django-admin.py runserver_plus
```

This will spawn the development server at `127.0.0.1:8000`. Moving to the next
terminal, we will want to run Gulp to watch our files:

```
$ gulp
```

The default Gulp task is the watch Task. This task will build the application
with Browserify, Compile SASS files, Compile CoffeeScript files, gather Third
Party CSS and Fonts, and listen for changes to SCSS, CoffeeScript, and Django
Template files. Upon change, the server will reload with BrowserSync.

The third terminal I personally like to use for inspecting the project and
running Vim.

**Troubleshooting**
If you have problems with ENOSPC it means it cannot compile that many files at once, to fix this in linux

```
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
```

### Editing the Settings

In the case that you want to change settings, edit the frontend things, or
customize it, the following guide will help.

#### Django Settings

The Django settings are located in the `{{project_name}}/settings/` directory. They
inherit from one another with the final output being:

```
      -- Development --
     /                  \
Base ---  Production  --- Local
     \                  /
      --   Staging   --
```

The three centerpieces are interchangable based on the current environment you
are working in. In most cases, you will edit an environment file. The
`local.py` file will be for all secret variables as it is referenced in the
`.gitignore`.

#### Gulp Config

The Gulp master config is a JavaScript Object that lives in `gulp/config.js`.
In here, we have a reference to all build paths which should all just work. If
you want to add new packages for CSS and Fonts though, you will need to
reference the `config.third_party` attribute. You can use the examples from
Bootstrap and Font-Awesome as a guide.

This is done here as there is no good way to include these files into the
build process like I wanted. NOTICE we are not using Bower for files. NPM has
become a full package manager for all frontend tools and I believe having
multiple package managers for frontend is bloat.

When you have thrird part JS files, you should be able to `require()` them
within `{{project_name}}/dev/third-party/dependencies.js`. Browserify knows of this
file and will grab the scripts from `node_modules/` and will generate a single
file for us.

All active development for the frontend should be done within the
`{{project_name}}/dev/` directory.

### Gulp Tasks

The following Gulp tasks can all be run:

```
$ gulp                      - Will run watcher
$ gulp build                - Will build dependencies and run watchify
$ gulp build --production   - Will build dependencies and exit on finish
$ gulp browserify           - Will only run browserify
$ gulp coffee               - Will only compile coffeescript into JS
$ gulp imageOptimize        - Will optimize all images
$ gulp sass                 - Will only compile scss into css
$ gulp templates            - Will only reload browsersync
$ gulp third_party          - Will only bundled specified third-party css and fonts
$ gulp watch                - Will run watcher
```

Typically, you should stick with only two commands: `gulp` and `gulp build
--production`. These tasks will be most efficient.

## Running Tests

Tests are currently ready for Python code based on pytest and coverage. To run
just the test suite:

```
$ py.test
```

In order to run the tests and get a coverage report:

```
$ bash bin/coverage.sh
```

These two tasks will give you a detailed output of the Python code.