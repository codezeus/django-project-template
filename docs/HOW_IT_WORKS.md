# How The App Works

This is a brief rundown on how the application should work for a user. It
gives the general idea of the features available and how they will interact
with the system.

## Registration

The user will be able to register for the application by simply providing an
email, and a password. Initially, their profile will generate a UUID for their
username and the user can then update that username within their profile to
something easier to find. This simple registration will be good for onboarding
users quickly.

We may also (and probably will) consider social login. Facebook possibly and
maybe Google as well.

When this is a paid service, we will notify the user that they have a 30 day
free trial period.

## Login

Logging in will also take an email and a password simply. We will also allow
social login probably.

## User Profile

The user profile will have a few sections. The first section is about the mom
or parent in general. They can:

- Fill in their name
- Set a username
- Upload their photo
- Define their personality, location, ethnicity, etc

The second section will be for a parent's children. They can add new children
and update their other children. Children will have:

- An age
- A gender
- A name

The third section will deal with settings which will have:

- Show email
- Receive emails for <ACTION>

There may be other parts of the profile that we can think of, but for now this
will be good.

## Groups

Parents will have the option of joining and creating groups. Groups may be
private or public. For public groups, people can openly join them and
participate in them. When they are part of a group, they will be able to see
group updates and and group events as they appear.

The owner of a group can choose to add certain users as admins when they edit
the group. A list of users who are in the group will appear and the owner can
select who they want as admins.

Groups will have a Facebook-like wall with group posts. Posts can have
comments also like Facebook.

Private events will allow users to invite other users. Ultimately, an admin
will need to approve added users. This will be an added layer of privacy for a
private group.

## Events

Events can be private or public as well. They are very similiar to groups in
how you can manage them and post to them. The only real difference is that
these have a location and once they happen then they are closed and archived
essentially.

## Friends

Users will be able to create a friends list so they can have quick access to
people they have met up with or who they know. Friendships start as requests.

## Report Users

Users can report other users if they believe they are suspicious. This will
allow moms to alert us that a person's activity should be monitored in case
they aren't who they say they are.

## Searching

Parents will be able to search and find users based on:

- By Kids Age Group
- &|| By Location
- &|| Mom Age
- &|| Distance

They will also be able to search for events based on:

- Location
- Distance

## Calendar

Events will be able to be displayed on a calendar to make things easy to
visualize.

## Emails

We'll send emails to people to remind them of upcoming events, invites,
friendships, etc. These can be turned off in settings.
